# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date


class send_mail_tempcandidate(models.Model):
    _name = 'send_mail_tempcandidate.send_mail_tempcandidate'

    @api.model
    def _cron_do_task(self):
        sql = "select rp.name, count(mc.id), '" + str(
            date.today()) + "', COALESCE(sum(bt.baito), 0),count(mc.id) - COALESCE(sum(bt.baito), 0)" \
                            " from res_users as ru left join res_partner as rp on ru.partner_id = rp.id" \
                            " left join manage_candidate_manage_candidate as mc on mc.create_uid = ru.id" \
                            " and mc.create_date::TIMESTAMP::DATE = '" + str(date.today()) + "'" \
                            " left join (select id, count(id) as baito from " \
                            " manage_candidate_manage_candidate where tag like '%baito%' or tag like '%Baito%' " \
                            " group by id) as bt" \
                            " on bt.id= mc.id" \
                            " group by rp.name ,mc.create_date::TIMESTAMP::DATE;"
        self._cr.execute(sql)
        id_needed = self._cr.fetchall()
        data_mj = []
        for item in range(len(id_needed)):
            if(id_needed[item][1] != 0):
                data_mj.append({
                    '0': str(id_needed[item][0]),
                    '1': str(id_needed[item][1]),
                    '2': int(id_needed[item][3]),
                    '3': int(id_needed[item][4]),
                    '3': int(id_needed[item][4]),
                })

        string = '<tr style ="border-collapse: collapse;"><td style="text-align: center;border:1px solid black; border-collapse: collapse;font-family: TimesNewRoman" colspan="5">Ngày ' + str(
            id_needed[0][2]) + '</td>' \
                               '</tr>'
        string += '<tr style ="border-collapse: collapse;">' \
                  '<td style="text-align: center;border:1px solid black; border-collapse: collapse; font-family: TimesNewRoman" rowspan="2">STT</td>' \
                  '<td style="text-align: center;border:1px solid black; border-collapse: collapse; font-family: TimesNewRoman" rowspan="2">Tên nhân viên</td>' \
                  '<td style="text-align: center;border:1px solid black; border-collapse: collapse; font-family: TimesNewRoman" colspan = "3">Số lượng CV</td>' \
                  '</tr>' \
                  '<tr style ="border-collapse: collapse;">' \
                  '<td style="text-align: center;border:1px solid black; border-collapse: collapse; font-family: TimesNewRoman" >Baito</td>' \
                  '<td style="text-align: center;border:1px solid black; border-collapse: collapse; font-family: TimesNewRoman" >Khác</td>' \
                  '<td style="text-align: center;border:1px solid black; border-collapse: collapse; font-family: TimesNewRoman" >Tổng</td>' \
                  '</tr>'
        i = 0;
        for d in data_mj:
            i = i + 1;
            string += '<tr style ="border-collapse: collapse;"><td style="text-align: center;border:1px solid black; border-collapse: collapse; font-family: TimesNewRoman" >' + str(
                i) + '</td> '
            string += '<td style="text-align: center;border:1px solid black; border-collapse: collapse; font-family: TimesNewRoman" >' + str(
                d.get('0')) + '</td> '
            string += '<td style="text-align: center;border:1px solid black; border-collapse: collapse; font-family: TimesNewRoman" >' + str(
                d.get('2')) + '</td> '
            string += '<td style="text-align: center;border:1px solid black; border-collapse: collapse; font-family: TimesNewRoman" >' + str(
                d.get('3')) + '</td> '
            string += '<td style="text-align: center;border:1px solid black; border-collapse: collapse; font-family: TimesNewRoman" >' + str(
                d.get('1')) + '</td></tr> '

        mail = self.env.ref('mail_template.example_email_template_candidate')
        origin_mail = mail.body_html
        mail.body_html = origin_mail.replace('{data_candidate}', string)
        return origin_mail

    @api.model
    def cron_do_task(self):
        origin_mail = self._cron_do_task()
        template = self.env.ref('mail_template.example_email_template_candidate')
        # origin_mail_begin = template.body_html
        # template.body_html = origin_mail
        print(template.id)
        ketqua = template.send_mail(1, force_send=True)
        print(ketqua)
        template.body_html = origin_mail
