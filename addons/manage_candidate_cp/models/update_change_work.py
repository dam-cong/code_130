# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date


class update_change_work(models.Model):
    _name = 'update_change_work'

    @api.model
    def update_change(self):
        sql_month = "select date_part('month',TIMESTAMP '"+str(date.today())+"')"
        self._cr.execute(sql_month)
        id_needed = self._cr.fetchall()
        # print(int(id_needed[0][0]))
        if int(id_needed[0][0]) < 10:
            sql = "update manage_candidate_manage_candidate " \
                  "set change_work = 0 " \
                  "where date_part('month',date_change_work) + 3= "+int(id_needed[0][0])+";"
            self._cr.execute(sql)
        elif int(id_needed[0][0]) == 10:
            sql_10 = "update manage_candidate_manage_candidate " \
                    "set change_work = 0 " \
                    "where date_part('month',date_change_work) + 3= 13;"
            self._cr.execute(sql_10)
        elif int(id_needed[0][0]) == 11:
            # print('12a')
            sql_11 = "update manage_candidate_manage_candidate " \
                    "set change_work = 0 " \
                    "where date_part('month',date_change_work) + 3= 14;"
            # print((sql_11))
            self._cr.execute(sql_11)
        elif int(id_needed[0][0]) == 12:
            sql_12 = "update manage_candidate_manage_candidate " \
                    "set change_work = 1 " \
                    "where date_part('month',date_change_work) + 3= 15;"
            self._cr.execute(sql_12)