# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date
from odoo.exceptions import ValidationError
from pytz import timezone

from fuzzysearch import find_near_matches

class manage_candidate(models.Model):
    _name = 'manage_candidate.manage_candidate'
    _rec_name = 'name_candidate'
    _order = 'order_level desc, order_search desc'
    _sql_constraints = [
        ('uni', 'unique(update_infor)', 'Đã tồn tại link facebook hoặc số điện thoại ứng viên!')]
    name_candidate = fields.Char("Tên ứng viên(*)")
    birthday = fields.Char("Năm sinh(*)")
    @api.one
    @api.depends('birthday')
    def _older_cand(self):
        ye = date.today()
        if self.birthday != 0:
            old = int(ye.year) - int(self.birthday)
            self.old = old
    old = fields.Integer(compute='_older_cand', store= True)

    sex = fields.Selection([('Nam', 'Nam'), ('Nữ', 'Nữ')], string="Giới tính")
    telephone_number = fields.Char("Số ĐT")
    email = fields.Char("Email")
    link_fb = fields.Char("Link FB")
    update_infor = fields.Char(compute="_updateinf",store= True)
    @api.one
    @api.depends('telephone_number', 'link_fb')
    def _updateinf(self):
        self.update_infor = str(self.telephone_number) + str(self.link_fb)
    home = fields.Many2one('manage_candidate.address', string="Quê quán")
    home_now = fields.Many2one('manage_candidate.address', string="Địa chỉ hiện tại")
    home_now_country = fields.Char(related='home_now.country.name')
    school = fields.Many2one('manage_candidate.school', string="Trường đào tạo")
    specialized = fields.Many2one('manage_candidate.specialized', string="Chuyên ngành")
    language = fields.Many2one('manage_candidate.language', "Ngoại ngữ")
    level = fields.Many2one('manage_candidate.level', "Trình độ")
    degree = fields.Selection([('Có', 'Có'), ('Không', 'Không')], string="Chứng chỉ")
    name_job = fields.Many2one('manage_job.manage_job', string="Lĩnh vực ngành nghề")
    hope_address = fields.Many2one('manage_candidate.address', string="Muốn làm việc ở")
    orther = fields.Many2one('manage_job.manage_job', string="Lĩnh vực/Khác")
    time_on_home = fields.Date(string="Thời gian về nước")
    experience = fields.Char(string="Kinh nghiệm")
    experience_job1 = fields.Many2one('manage_job.manage_job',string="Kinh nghiệm")
    experience_time1 = fields.Many2one('manage_experience_time',string="Thời gian")
    experience_job2 = fields.Many2one('manage_job.manage_job')
    experience_time2 = fields.Many2one('manage_experience_time')
    experience_job3 = fields.Many2one('manage_job.manage_job')
    experience_time3 = fields.Many2one('manage_experience_time')
    file = fields.Many2many('manage_candidate.file', string="Hồ sơ")
    note = fields.Text("Ghi chú")
    salary_now = fields.Float(string="Hiện tại")
    salary_hope = fields.Float(string="Mong muốn")
    upload_file = fields.Binary(string="Upload File")
    file_name = fields.Char(string="File Name")
    order_search = fields.Integer(default = 0)
    change_work = fields.Integer(default = 0)
    date_change_work = fields.Date()
    # order_lan = fields.Integer(compute = '_or_lan', store = True)
    order_lan = fields.Integer()

    def change_work_button(self):
        self.change_work = 1
        self.date_change_work = date.today()

    def change_work_cancel(self):
        self.change_work = 0

    @api.one
    @api.depends('name_candidate', 'telephone_number', 'email', 'school', 'specialized', 'level', 'language',
                 'name_job', 'orther', 'experience', 'note')
    def _tag_search(self):
        strt = str(self.name_candidate) + str(self.language.name) + str(self.note) + str(self.telephone_number) + str(
            self.email) + str(self.school.name) \
               + str(self.specialized.name) + str(self.level.name) + str(self.name_job.name) + str(
            self.orther.name) + str(self.experience)
        self.tag = str(strt)

    tag_ao = fields.Text(compute='_tag_search')
    tag = fields.Text()
    time_on_home_required = fields.Integer()

    @api.onchange('name_job')
    def _timeon(self):
        time_on_home_requireds = self.env['manage_job.manage_job'].search(
            [('name', 'ilike', 'quay lại'), ('id', '=', self.name_job.id)], limit=1)
        self.time_on_home_required = time_on_home_requireds.id

    key_world_can = fields.Char("Từ khóa tìm kiếm")
    key_khuvuc_can = fields.Many2one('manage_candidate.address', "Khu vực")
    key_khuvuc_can_country = fields.Char(related='key_khuvuc_can.country.name')
    key_chuyenmon_can = fields.Many2one('chuyenmon', string="Lĩnh vực/chuyên môn")
    key_tieng_can = fields.Many2one('manage_candidate.language', string="Ngoại ngữ")
    key_tieng_can_level = fields.Many2one('manage_candidate.level', string="Trình độ")
    @api.one
    @api.depends('key_tieng_can_level')
    def _order_level(self):
        self.order_level = int(self.key_tieng_can_level.order_level)

    order_level = fields.Integer(compute ='_order_level', store = True)

    key_salary = fields.Char(string="Mức lương")
    salary_hope_se = fields.Selection([('yen', 'Yên'), ('vnd', 'VNĐ'), ('do', '$')], required=True)
    # key_address = fields.Many2one('manage_candidate.address', string="Địa chỉ hiện tại")
    key_address_country = fields.Many2one('country', string="Địa chỉ hiện tại")
    key_khac_can = fields.Many2one('khac', "Khác")

    @api.model
    def create(self, vals):
        record = super(manage_candidate, self).create(vals)
        self.env['candidate_jobs'].create({'candidate_id': record.id})
        return record

    jobs_promoted = fields.Many2many('jobs.jobs',compute='_compute_job_promoted',
                                         string="Job đã tiến cử")
    @api.one
    def _compute_job_promoted(self):
        related_ids = []
        stri = str(self.id)
        if(self.id):
            sql = "SELECT jj.id "\
                  "FROM candidate_jobs as cj , candidate_jobs_jobs_jobs_rel as cjj, jobs_jobs as jj "\
                  "where cj.id = cjj.candidate_id and cjj.job_id = jj.id "\
                  "and promoted = true and cj.candidate_id = " + str(self.id)
            self._cr.execute(sql)
            id_needed = self._cr.fetchall()
            self.jobs_promoted = self.env['jobs.jobs'].search([('id', 'in', id_needed)])

    job_cand_recomment = fields.Many2many('jobs.jobs', relation="jobs_jobs_manage_candidate_manage_candidate_rel", column1="manage_candidate_manage_candidate_id",
                                  column2="jobs_jobs_id",string="Gợi ý job")

    @api.multi
    def search_jobs(self):
        jobs = self.env['jobs.jobs'].sudo().search([], order='order_level_job desc')
        for tags in jobs:
            order = 1
            if self.key_chuyenmon_can:
                if tags.key_chuyenmon == self.key_chuyenmon_can:
                    order = order * 3
            if self.key_khuvuc_can:
                if tags.key_khuvuc == self.key_khuvuc_can:
                    order = order * 5
            if self.key_tieng_can:
                if int(self.order_level) >= int(tags.order_level_job):
                    order = order * 7
            if self.key_salary:
                if self.salary_hope_se:
                    if self.salary_hope_se == 'yen':
                        if float(self.key_salary) >= float(tags.salary_from_yen) and float(self.key_salary) <= float(tags.salary_to_yen):
                            order = order * 11
                    elif self.salary_hope_se == 'vnd':
                        if float(self.key_salary) >= float(tags.salary_from_vnd) and float(self.key_salary) <= float(tags.salary_to_vnd):
                            order = order * 11
                    else:
                        if float(self.key_salary) >= float(tags.salary_from_do) and float(self.key_salary) <= float(tags.salary_to_do):
                            order = order * 11
            # if self.key_khuvuc_tuyen:
            #     if self.key_khuvuc == tags.key_khuvuc_can:
            #         order = order * 13
            if self.key_khac_can:
                if self.key_khac_can == tags.key_khac:
                    order = order * 13
            # print(order)
            if order % 15015 == 0:
                tags.order_search_can = 10
            elif order % 1155 == 0:
                tags.order_search_can = 9
            elif order % 231 == 0:
                tags.order_search_can = 8
            elif order % 21 == 0:
                tags.order_search_can = 7
            else:
                tags.order_search_can = 1

        self.job_cand_recomment = self.env['jobs.jobs'].search([('order_search_can', '>', 1)],
                                                                order='order_search_can desc')

class school(models.Model):
    _name = 'manage_candidate.school'
    _rec_name = 'name'
    name = fields.Char()
    _sql_constraints = [
        ('school_uni', 'unique (name)', 'Trường đã tồn tại!')]

class specialized(models.Model):
    _name = 'manage_candidate.specialized'
    _rec_name = 'name'
    name = fields.Char()
    _sql_constraints = [
        ('uni_special', 'unique (name)', 'Chuyên ngành đã tồn tại!')]

class language(models.Model):
    _name = 'manage_candidate.language'
    _rec_name = 'name'
    name = fields.Char()
    _sql_constraints = [
        ('uni_lan', 'unique (name)', 'Ngoại ngữ đã tồn tại!')]

class level(models.Model):
    _name = 'manage_candidate.level'
    _rec_name = 'name'
    name = fields.Char(string = "Trình độ")
    language_name = fields.Many2one('manage_candidate.language', "Ngôn ngữ")
    order_level = fields.Selection([(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10)], string = "Phân cấp")
    _sql_constraints = [
        ('uni_level', 'unique (name)', 'Đã tồn tại!')]

class manage_file(models.Model):
    _name = 'manage_candidate.file'
    _rec_name = 'name'
    name = fields.Char()
    _sql_constraints = [
        ('uni_file', 'unique (name)', 'Đã tồn tại!')]

class manage_orther(models.Model):
    _name = 'manage_candidate.orther'
    _rec_name = 'name'
    name = fields.Char()
    _sql_constraints = [
        ('uni_orther', 'unique (name)', 'Đã tồn tại!')]
class countr(models.Model):
    _name = 'country'
    _rec_name = 'name'
    name = fields.Char()
    _sql_constraints = [
        ('uni_countr', 'unique (name)', 'Tỉnh đã tồn tại!')]
class manage_address(models.Model):
    _name = 'manage_candidate.address'
    _rec_name = 'name'
    name = fields.Char(string="Tỉnh")
    country = fields.Many2one('country', string="Nước")
    _sql_constraints = [
        ('uni_addr', 'unique (name)', 'Đã tồn tại!')]

class manage_job(models.Model):
    _name = 'manage_job.manage_job'
    _rec_name = 'name'
    name = fields.Char("Nguyện vọng")
    _sql_constraints = [
        ('uni_mjob', 'unique (name)', 'Đã tồn tại!')]
class manage_experience(models.Model):
    _name = 'manage_experience'
    _rec_name = 'name'
    name = fields.Char("Kinh nghiệm")
    _sql_constraints = [
        ('uni_exper', 'unique (name)', 'Đã tồn tại!')]
class manage_experience_time(models.Model):
    _name = 'manage_experience_time'
    _rec_name = 'name'
    name = fields.Char("Thời gian")
    _sql_constraints = [
        ('uni_extime', 'unique (name)', 'Đã tồn tại!')]