# -*- coding: utf-8 -*-

from odoo import models, fields, api
from tempfile import TemporaryFile, NamedTemporaryFile
from docx import Document
from docxtpl import DocxTemplate, InlineImage
from io import BytesIO, StringIO
import codecs
import os
import logging

from fuzzysearch import find_near_matches
from passlib.handlers.oracle import oracle10

_logger = logging.getLogger(__name__)
class jobs(models.Model):
    _name = 'jobs.jobs'
    _rec_name = 'name'
    _order = 'order_search_can desc,order_level_job desc'

    # manage_candidate = fields.Many2one("manage_candidate.manage_candidate","Ứng viên")
    # _sql_constraints = [
    #     ('name', 'unique (name)', 'Đã tồn tại!')]
    name = fields.Char("Tên job")
    address = fields.Many2many('manage_candidate.address', string="Khu vực công ty")
    address_country = fields.Char(related='address.country.name')
    address_ga = fields.Char(string="Ga gần nhất (Dành cho các job baito)")
    from_company = fields.Many2one('jobs.company_jobs', string="Từ công ty có jobs")
    # type_jobs = fields.Many2one('jobs.type_jobs', string="Chủng loại jobs")
    type_jobs = fields.Selection([('A', 'A'),('B', 'B'), ('C', 'C'), ('D', 'D'),('def', 'Khác')], string="Chủng loại jobs")
    charge_jobs = fields.Many2one('jobs.charge_jobs', string="Người phụ trách jobs")

    amount = fields.Many2one('jobs.amount', "Số lượng")
    amount_recruitment = fields.Char("Số lượng tuyển", relate='amount.amount_recruitment')
    amount_exam = fields.Char("Số lượng thi tuyển", relate='amount.amount_exame')

    older = fields.Char("Độ tuổi")
    sex_required = fields.Selection([('Nam','Nam'),('Nữ','Nữ'),('Nam và nữ', 'Nam và nữ')],"Yêu cầu giới tính")
    work_content = fields.Many2one('jobs.workcontent', "Nội dung công việc")

    time_work = fields.Selection([('1', 'Hành chính'),('2', 'Ca')], string="Thời gian làm việc")
    time_work_note = fields.Char()
    worker_time = fields.Char(compute ="_worker_time", store = True)
    @api.one
    @api.depends('time_work','time_work_note')
    def _worker_time(self):
        if self.time_work == '1':
            self.worker_time = str("Hành chính ") + str(self.time_work_note)
        if self.time_work == '2' :
            self.worker_time = str("Ca ") + str(self.time_work_note)
    overtime = fields.Char(string="Làm thêm (Tổng số giờ/tháng)")

    benifit = fields.Many2one('jobs.benifit', "Chế độ quyền lợi", limit =  1)
    salary_from = fields.Char()
    salary_to = fields.Char()
    salary_from_vnd = fields.Char()
    salary_to_vnd = fields.Char()
    salary_from_yen = fields.Char()
    salary_to_yen = fields.Char()
    salary_from_do = fields.Char()
    salary_to_do = fields.Char()
    salary_se = fields.Selection([('yen','Yên'),('vnd','VNĐ'), ('do', '$')], required= True)
    salary_unit = fields.Float(default = 200)
    salary_unit_do = fields.Float(default = 109)
    salary_unit_vnd = fields.Float(default = 23195)
    @api.one
    @api.depends('salary_from','salary_to', 'salary_se')
    def _salary_string(self):
        if self.salary_se:
            if self.salary_se == 'yen':
                self.salary_from_vnd = float(self.salary_from) * float(self.salary_unit)
                self.salary_to_vnd = float(self.salary_to) * float(self.salary_unit)
                self.salary_from_yen = self.salary_from
                self.salary_to_yen = self.salary_to
                self.salary_from_do = float(self.salary_from) / float(self.salary_unit_do)
                self.salary_to_do = float(self.salary_to) / float(self.salary_unit_do)
                self.salary = str(self.salary_from) + str(" - ") + str(self.salary_to) + str("Yên")
            elif self.salary_se == 'vnd':
                self.salary_from_vnd = self.salary_from
                self.salary_to_vnd = self.salary_to
                self.salary_from_yen = float(self.salary_from) / float(self.salary_unit)
                self.salary_to_yen = float(self.salary_to) / float(self.salary_unit)
                self.salary_from_do = float(self.salary_from) / float(self.salary_unit_vnd)
                self.salary_to_do = float(self.salary_to) / float(self.salary_unit_vnd)
                self.salary = str(self.salary_from) + str(" - ") + str(self.salary_to) +str("VNĐ")
            else:
                self.salary_from_do = self.salary_from
                self.salary_to_do = self.salary_to
                self.salary_from_yen = float(self.salary_from) * float(self.salary_unit_do)
                self.salary_to_yen = float(self.salary_to) * float(self.salary_unit_do)
                self.salary_from_vnd = float(self.salary_from) * float(self.salary_unit_vnd)
                self.salary_to_vnd = float(self.salary_to) * float(self.salary_unit_vnd)
                self.salary = str(self.salary_from) + str(" - ") + str(self.salary_to) + str("$")

    salary = fields.Char("Lương", compute="_salary_string", strore= True)
    reward = fields.Char("Thưởng")
    subsidize = fields.Char("Trợ cấp")
    insurance = fields.Char("Bảo hiểm")
    diff_benifit = fields.Char("Chế độ khác")

    required = fields.Many2one('jobs.required',"Yêu cầu công việc")
    language = fields.Char("Trình độ tiếng")
    education = fields.Char("Trình độ học vấn")
    diff_required = fields.Text("Yêu cầu khác")

    date_recomment = fields.Date("Ngày dự kiến chốt form")
    date_exam = fields.Date("Ngày dự kiến thi tuyển")
    date_sign = fields.Date("Thời gian kí hợp đồng dự kiến")
    date_expectfly = fields.Date("Thời gian xuất cảnh dự kiến")

    note = fields.Many2one('jobs.note',"Ghi chú")
    # tag_ao = fields.Text(compute='_tag_search_jobs')
    # tag_ao = fields.Text(compute = '_tag_jobs')
    tag = fields.Text(compute = '_tag_jobs', store=True)
    order_search_can = fields.Integer(default=0)

    # @api.one
    # def _nana(self):
    #     self.candidates_job = self.env['candidate_jobs'].search([])

    def domain_interns_clone(self):
        intern_clone = self.env['candidate_jobs'].search([('promoted', '=', False)]).ids
        domain = "[('id', 'in', %s)]" % str(intern_clone)
        return domain
    candidates_job = fields.Many2many('candidate_jobs', relation="candidate_jobs_jobs_jobs_rel", column1="job_id",
                                  column2="candidate_id", string="Ứng viên", domain=domain_interns_clone)  # hh_184
    candidates_job_promoted = fields.Many2many('candidate_jobs', relation="candidate_jobs_jobs_jobs_rel", column1="job_id",
                                  column2="candidate_id", string="Ứng viên", domain=[('promoted', '=', True)])  # hh_184


    @api.one
    @api.depends('name', 'address','from_company','charge_jobs', 'work_content','benifit','diff_benifit','required','language','education','diff_required','note',)
    def _tag_jobs(self):
        strt = str(self.name) + str(self.address)+ str(self.from_company.name) + str(self.charge_jobs.name) + \
               str(self.work_content.name) + str(self.benifit.name)+ str(self.diff_benifit)+ str(self.required.name)\
               + str(self.language)+ str(self.education)+ str(self.note.name)
        self.tag = str(strt)

    key_world = fields.Char("Từ khóa tìm kiếm")
    key_khuvuc = fields.Many2one('manage_candidate.address', "Ứng viên muốn làm việc tại")
    key_khuvuc_country = fields.Char(related='key_khuvuc.country.name')
    key_chuyenmon = fields.Many2one('chuyenmon',string = "Lĩnh vực ngành nghề", required = True)
    key_tieng = fields.Many2one('manage_candidate.language', string="Ngoại ngữ")
    key_tieng_level = fields.Many2one('manage_candidate.level', string="Trình độ" , required = True)
    @api.one
    @api.depends('key_tieng_level')
    def _order_level_job(self):
        self.order_level_job = int(self.key_tieng_level.order_level)

    order_level_job = fields.Integer(compute='_order_level_job', store=True)
    key_khuvuc_tuyen = fields.Many2one('country', string="Khu vực tuyển")
    key_khac = fields.Many2one('khac',"Khác")
    # jobs_candidate = fields.One2many(comodel_name="jobs_candidate", inverse_name="jobs_id",
    #
    #                                 string="Lý lịch làm việc")
    # @api.multi
    # def url_jobs(self):
    #     return {
    #         'type': 'ir.actions.act_url',
    #         'url': '/web/binary/render_CV_list_page?id=%s' % (str(self.id)),
    #         'target': 'self', }
    @api.multi
    def search_candidate(self):
        cvs = self.env['candidate_jobs'].sudo().search([('promoted', '=', False)], order = 'order_level desc')
        for tags in cvs:
            order = 1
            if int(tags.change_work) == 1:
                order = order * 3
            arr = []
            if self.key_khuvuc:
                if tags.key_khuvuc_can == self.key_khuvuc:
                    order = order * 5
            if self.key_chuyenmon:
                # i =  0
                # for item in self.key_chuyenmon:
                #     arr.append(item.id)
                #     if arr[i] == tags.key_chuyenmon_can.id:
                #         print(tags.key_chuyenmon_can.id)
                #         order = order * 3
                #         print(order)
                #     i = i +1
                if tags.key_chuyenmon_can == self.key_chuyenmon:
                    order = order * 7
            if self.key_tieng:
                if int(self.order_level_job) <= int(tags.order_level):
                        order = order * 11
            if self.key_khuvuc_tuyen:
                if self.key_khuvuc == tags.key_khuvuc_can:
                    order = order * 13
            if self.key_khac:
                if self.key_khac == tags.key_khac_can:
                    order = order * 17
            if order%255255== 0:
                tags.order_search = 10
            elif order%15015== 0 or order%19635 == 0:
                tags.order_search = 9
            elif order%1155== 0:
                tags.order_search = 8
            elif order%231== 0:
                tags.order_search = 7
            elif order%85085== 0:
                tags.order_search = 6
            elif order%5005== 0 or order%6545 == 0:
                tags.order_search = 5
            elif order % 385 == 0:
                tags.order_search = 4
            elif order % 77 == 0:
                tags.order_search = 3
            else:
                tags.order_search = 1
        sql = "select id from candidate_jobs where promoted = False and order_search > 1"
        print(sql)
        self.candidates_job = self.env['candidate_jobs'].search([('promoted', '=', False),('order_search', '>', 1)], order = 'order_search desc')
    order_search_jobs = fields.Integer(default=0)

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res

    print_world = fields.Many2many('jobs.print_world', string="In world")
    doc = fields.Many2one('jobs.document',"Tải world")

    @api.multi
    def confirm_request(self):
        _logger.info("confirm")
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download_jobs?model=jobs.jobs&id=%s&filename=%s.zip&doc=%s' % (
                str(self.id), str(self.name), str(self.doc.name)),
            'target': 'self', }

    def createWorldDoc(self):
        # bản đầy đủ
        docs = self.env['jobs.document'].search([('name', '=', "world")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            tpl = DocxTemplate(stream)
            context = {}
            context['name'] = self.name
            if len(self.address) > 1:
                str_add = ""
                for item in self.address:
                    str_add = str(str_add) + " " + str(item.name)
                print(str_add)
                context['address'] = str(str_add)
            else:
                context['address'] = str(self.address)
            context['address_ga'] = str(self.address_ga)
            context['amount_recuiment'] = self.amount_recruitment
            context['amount_exam'] = self.amount_exam
            context['older'] = self.older
            context['sex_required'] = self.sex_required
            context['work_content'] = self.work_content.name
            context['salary'] = self.salary
            context['reward'] = self.reward
            context['subsidize'] = self.subsidize
            context['insurance'] = self.insurance
            context['diff_benifit'] = self.benifit.name
            context['language'] = self.language
            context['education'] = self.education
            context['diff_required'] = self.diff_required
            context['date_recomment'] = self.date_recomment
            context['date_exam'] = self.date_exam
            context['date_expectfy'] = self.date_expectfly
            tpl.render(context)

            tempFile = NamedTemporaryFile(delete=False)
            tpl.save(tempFile)
            tempFile.flush()
            tempFile.close()

            return tempFile

    def createWorldDoc2(self):
        # bản thiếu thưởng và trợ cấp
        docs = self.env['jobs.document'].search([('name', '=', "world2")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            tpl = DocxTemplate(stream)
            context = {}
            context['name'] = self.name
            if len(self.address) > 1:
                str_add = ""
                for item in self.address:
                    str_add = str(str_add) + " " + str(item.name)
                print(str_add)
                context['address'] = str(str_add)
            else:
                context['address'] = str(self.address)
            context['address_ga'] = str(self.address_ga)
            context['amount_recuiment'] = self.amount_recruitment
            context['amount_exam'] = self.amount_exam
            context['older'] = self.older
            context['sex_required'] = self.sex_required
            context['work_content'] = self.work_content.name
            context['salary'] = self.salary

            context['insurance'] = self.insurance
            context['diff_benifit'] = self.benifit.name
            context['language'] = self.language
            context['education'] = self.education
            context['diff_required'] = self.diff_required
            context['date_recomment'] = self.date_recomment
            context['date_exam'] = self.date_exam
            context['date_expectfy'] = self.date_expectfly
            tpl.render(context)

            tempFile = NamedTemporaryFile(delete=False)
            tpl.save(tempFile)
            tempFile.flush()
            tempFile.close()

            return tempFile

    def createWorldDoc3(self):
        # bản thiếu thưởng
        docs = self.env['jobs.document'].search([('name', '=', "world3")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            tpl = DocxTemplate(stream)
            context = {}
            context['name'] = self.name
            if len(self.address) > 1:
                str_add = ""
                for item in self.address:
                    str_add = str(str_add) + " " + str(item.name)
                print(str_add)
                context['address'] = str(str_add)
            else:
                context['address'] = str(self.address)
            context['address_ga'] = str(self.address_ga)
            context['amount_recuiment'] = self.amount_recruitment
            context['amount_exam'] = self.amount_exam
            context['older'] = self.older
            context['sex_required'] = self.sex_required
            context['work_content'] = self.work_content.name
            context['salary'] = self.salary
            context['subsidize'] = self.subsidize
            context['insurance'] = self.insurance
            context['diff_benifit'] = self.benifit.name
            context['language'] = self.language
            context['education'] = self.education
            context['diff_required'] = self.diff_required
            context['date_recomment'] = self.date_recomment
            context['date_exam'] = self.date_exam
            context['date_expectfy'] = self.date_expectfly
            tpl.render(context)

            tempFile = NamedTemporaryFile(delete=False)
            tpl.save(tempFile)
            tempFile.flush()
            tempFile.close()

            return tempFile

    def createWorldDoc4(self):
        # bản thiếu trợ cấp
        docs = self.env['jobs.document'].search([('name', '=', "world4")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            tpl = DocxTemplate(stream)
            context = {}
            context['name'] = self.name
            if len(self.address) > 1:
                str_add = ""
                for item in self.address:
                    str_add = str(str_add) + " " + str(item.name)
                print(str_add)
                context['address'] = str(str_add)
            else:
                context['address'] = str(self.address)
            context['address_ga'] = str(self.address_ga)
            context['amount_recuiment'] = self.amount_recruitment
            context['amount_exam'] = self.amount_exam
            context['older'] = self.older
            context['sex_required'] = self.sex_required
            context['work_content'] = self.work_content.name
            context['salary'] = self.salary
            context['reward'] = self.reward
            context['insurance'] = self.insurance
            context['diff_benifit'] = self.benifit.name
            context['language'] = self.language
            context['education'] = self.education
            context['diff_required'] = self.diff_required
            context['date_recomment'] = self.date_recomment
            context['date_exam'] = self.date_exam
            context['date_expectfy'] = self.date_expectfly
            tpl.render(context)

            tempFile = NamedTemporaryFile(delete=False)
            tpl.save(tempFile)
            tempFile.flush()
            tempFile.close()

            return tempFile
class company_jobs(models.Model):
    _name = 'jobs.company_jobs'
    _rec_name = 'name'

    name = fields.Char("Tên công ty")
    address = fields.Char("Địa chỉ")
    phone_number = fields.Char("Số điện thoại")
    post_office = fields.Char("Số bưu điện")
    fax_number = fields.Char("Số fax")
    representative = fields.Char("Người đại diện")
    responsible_person = fields.Char("Người chịu trách nhiệm")
    number_legal = fields.Char("Số pháp nhân")
    email = fields.Char("Email công ty")
    _sql_constraints = [
        ('uni_jcompany', 'unique (name)', 'Đã tồn tại!')]

class type_jobs(models.Model):
    _name = 'jobs.type_jobs'
    _rec_name = 'name'
    name = fields.Char("Loại jobs")
    _sql_constraints = [
        ('uni_jtype', 'unique (name)', 'Đã tồn tại!')]


class charge_jobs(models.Model):
    _name = 'jobs.charge_jobs'
    _rec_name = 'name'

    name = fields.Char("Tên người phụ trách jobs")
    id_number = fields.Char("Số chứng minh thư")
    address = fields.Char("Địa chỉ")
    phone_number = fields.Char("Số điện thoại")
    email = fields.Char("Email")
    _sql_constraints = [
        ('uni_jchar', 'unique (name)', 'Đã tồn tại!')]

class amount(models.Model):
    _name = 'jobs.amount'
    # _rec_name = 'name'
    # name = fields.Char("Số lượng", default = "Số lượng")
    amount_recruitment = fields.Integer("Số lượng tuyển")
    amount_exame = fields.Integer("Số lượng thi tuyển")

class benifit(models.Model):
    _name = 'jobs.benifit'
    _rec_name = 'name'
    name = fields.Text("Khác")
    # _sql_constraints = [
    #     ('uni_jbenifit', 'unique (name)', 'Đã tồn tại!')]

class required(models.Model):
    _name = 'jobs.required'
    _rec_name = 'name'
    name = fields.Text("Khác")
    # _sql_constraints = [
    #     ('u', 'unique (name)', 'Đã tồn tại!')]

class work_content(models.Model):
    _name = 'jobs.workcontent'
    _rec_name = 'name'
    name = fields.Text("Khác")
    # _sql_constraints = [
    #     ('name', 'unique (name)', 'Đã tồn tại!')]

class note(models.Model):
    _name = 'jobs.note'
    _rec_name = 'name'
    name = fields.Text("Khác")
    # _sql_constraints = [
    #     ('name', 'unique (name)', 'Đã tồn tại!')]

class print_world(models.Model):
    _name = 'jobs.print_world'
    _rec_name = 'name'
    name = fields.Char()
    # _sql_constraints = [
    #     ('name', 'unique (name)', 'Đã tồn tại!')]