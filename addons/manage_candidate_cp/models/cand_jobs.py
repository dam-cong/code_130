from odoo import models, fields, api
from datetime import date
from pytz import timezone

from fuzzysearch import find_near_matches

class candidate_jobs(models.Model):
    _name = 'candidate_jobs'
    _inherits = {'manage_candidate.manage_candidate': 'candidate_id'}
    _description = 'Danh sách ứng viên theo job'
    _order = 'order_search desc,order_level desc'
    # _rec_name = 'candidate_id'
    _sql_constraints = [
        ('name', 'unique (candidate_id,job_id)', 'Đã tồn tại ứng viên trong danh sách tiến cử!')]

    candidate_id = fields.Many2one('manage_candidate.manage_candidate', required=True, on_delete='restrict', auto_join=True,
                                string='Danh sách ứng viên', help='Intern-related data of the user', index=True)
    job_id = fields.Many2one('jobs.jobs',relation="candidate_jobs_jobs_jobs_rel",  column1="job_id",
                                  column2="candidate_id", string='Job')

    @api.model
    def create(self, vals):
        record = super(candidate_jobs, self).create(vals)
        return record
    promoted = fields.Boolean(string="Tiến cử")

    @api.multi
    def toggle_promoted(self):
        self.promoted = True
        model_B_obj = self.env['candidate_jobs'].create({'candidate_id': self.candidate_id.id})
        # related_ids = []
        # sql = "SELECT jj.id " \
        #       "FROM candidate_jobs as cj , candidate_jobs_jobs_jobs_rel as cjj, jobs_jobs as jj " \
        #       "where cj.id = cjj.candidate_id and cjj.job_id = jj.id " \
        #       "and cj.candidate_id = " + str(self.candidate_id) + " and promoted = true"
        # self._cr.execute(sql)
        # id_needed = self._cr.fetchall()
        # self.jobs_promoted = self.env['jobs.jobs'].search([('id', 'in', id_needed)])
        return model_B_obj
    interview = fields.Boolean(string = "Xác nhận phỏng vấn")
    join_interview = fields.Boolean(string = "Tham gia phỏng vấn")
    date_interview = fields.Date(string = "Ngày phỏng vấn")
    pass_interview = fields.Boolean(string = "Pass phỏng vấn")
    date_pass_interview = fields.Date(string = "Ngày có kết quả")
    worker_bol = fields.Boolean(string = "Đi làm")
    date_worker = fields.Date(string = "Ngày đi làm")