# -*- coding: utf-8 -*-

from odoo import models, fields, api

class search_chuyenmon(models.Model):
    _name = 'chuyenmon'
    _rec_name = 'name'
    name = fields.Char("Chuyên môn")
    _sql_constraints = [
        ('uni_chuyenmoon', 'unique (name)', 'Đã tồn tại!')]
class search_tieng(models.Model):
    _name = 'tieng'
    _rec_name = 'name'
    name = fields.Char("Ngôn ngữ")
    name_level = fields.Char("Trình độ")
    _sql_constraints = [
        ('uni_tieng', 'unique (name)', 'Đã tồn tại!')]
class search_khuvuc(models.Model):
    _name = 'khuvuc'
    _rec_name = 'name'
    name = fields.Char("Khu vực")
    _sql_constraints = [
        ('uni_khuvuc', 'unique (name)', 'Đã tồn tại!')]
class search_khac(models.Model):
    _name = 'khac'
    _rec_name = 'name'
    name = fields.Char("Khác")
    _sql_constraints = [
        ('unikhac', 'unique (name)', 'Đã tồn tại!')]
class search_hope_job(models.Model):
    _name = 'nguyenvong'
    _rec_name = 'name'
    name = fields.Char()
    _sql_constraints = [
        ('uni_nguyenvong', 'unique (name)', 'Đã tồn tại!')]
class search_salary(models.Model):
    _name = 'luong'
    _rec_name = 'name'

    name = fields.Char()
    _sql_constraints = [
        ('uni_luong', 'unique (name)', 'Đã tồn tại!')]

class mydocument(models.Model):
    _name = 'jobs.document'
    _description = u'Các loại văn bản, báo cáo'
    _rec_name = 'name'

    name = fields.Char("Tên", required=True)  # hh_104
    name_view = fields.Char("Tên hiển thị", required = True)
    note = fields.Text("Ghi chú")  # hh_105
    attachment = fields.Binary('Văn bản mẫu', required=True)  # hh_106

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name_view))
        return res

    _sql_constraints = [
        ('uni_doc', 'unique (name)', 'Đã tồn tại!')]
class salary_unit(models.Model):
    _name = 'salary_unit'
    uint = fields.Float()
