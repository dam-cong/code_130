# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.addons.jobs.models import models
import logging
_logger = logging.getLogger(__name__)

class HrSearch(http.Controller):
    @http.route('/web/binary/render_jobs_list_page', type='http', auth='public', website=True)
    def render_jobs_list_page(self,id, **kwargs):
        request.env['manage_candidate.manage_candidate'].browse(int(id)).search_job()
        invoices = request.env['jobs.jobs'].sudo().search([('order_search_can','>',20)])
        return http.request.render('manage_candidate.job_list_page', {'invoices':invoices})

    @http.route('/web/binary/render_CV_list_page', type='http', auth='public', website=True)
    def render_CV_list_page(self, id, **kwargs):
        request.env['jobs.jobs'].browse(int(id)).search_candidate()
        invoices = request.env['manage_candidate.manage_candidate'].sudo().search([('order_search', '>', 1)])
        return http.request.render('manage_candidate.CV_list_page', {'invoices': invoices})
