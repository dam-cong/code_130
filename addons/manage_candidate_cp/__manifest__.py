# -*- coding: utf-8 -*-
{
    'name': "manage_candidate_cp",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','mail'],

    # always loaded
    'data': [
        'security/user_groups.xml',
        'security/ir.model.access.csv',
        'views/style.xml',
        'views/website.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/jobs_view.xml',
        'views/jobs_website.xml',
        'views/dulieuchung.xml',
        'views/dulieuchung2.xml',
        # 'views/key_search.xml',
        'views/update_change_work.xml',
        'views/cand_job_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}