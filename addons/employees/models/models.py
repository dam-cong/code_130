# -*- coding: utf-8 -*-

from odoo import models, fields, api

class employees(models.Model):
    _name = 'employees.employees'

    name = fields.Char("Họ và tên")
    department = fields.Many2one('employees.department',"Phòng ban")
    position =  fields.Selection([('Employee', 'Nhân viên'), ('Manager', 'Trưởng phòng')], string="Chức vụ")
    birthday = fields.Date("Ngày sinh")
    resuser = fields.Many2one('res.users',"Người dùng")
    stt = fields.Integer("Số thứ tự")
    # resuser_ao = fields.Many2one()

    @api.one
    @api.depends('resuser')
    def _usersdp(self):
        # strr = ""
        employee_rec = self.env['res.users'].search([('name', '=', self.resuser.name)], limit=1)
        self.email = str(employee_rec.login)
        # print(strr)
        # return str(strr)

    email = fields.Char("Email", compute='_usersdp', store= True)
    _sql_constraints = [
        ('email', 'unique (email)', 'Email đã được đăng kí tài khoản!')]
    # @api.onchange('email')
    # def _users(self):
    #     # print(self.id)
    #     if self.email:
    #         sql = "insert into res_partner(name, company_id, displayname)"\
    #                 "values(N'"+str(self.name)+"', 1, N'"+str(self.name)+"');"
    #         self._cr.execute(sql)
    #
    #         sql_id = "select id from res_partner order by id desc limit 1;"
    #         self._cr.execute(sql_id)
    #         id_needed = self._cr.fetchall()
    #         id_partner = id_needed[0][0]
    #         print(id_partner)
    #         sql_user = "INSERT INTO res_users(login, company_id, partner_id)"\
	#                    "VALUES ('"+str(self.email)+"',1,"+str(id_partner)+");"
    #         self._cr.execute(sql_user)
    #         # self.env.cr.rollback()
    #         print(sql_user)


    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class department(models.Model):
    _name = 'employees.department'
    name = fields.Char("Tên phòng ban")
    email = fields.Char("Email trưởng phòng (yêu cầu nhập giống email đăng nhập)")
    uloginid = fields.Integer("ID login", readonly = True)
    _sql_constraints = [
        ('email', 'unique (email)', 'Email đã được đăng kí tài khoản!')]
    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
    @api.onchange('email')
    def _user(self):
        ulogin = self.env['res.users'].search([('login', '=', self.email)], limit=1)
        self.uloginid = ulogin.id
