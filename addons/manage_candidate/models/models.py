# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date
from pytz import timezone

from fuzzysearch import find_near_matches

class manage_candidate(models.Model):
    _name = 'manage_candidate.manage_candidate'
    _rec_name = 'name_candidate'
    _order = 'order_search desc,order_lan desc'
    # id = fields.Int(domain="[('write_uid', '=',1)]")
    name_candidate = fields.Char("Tên ứng viên(*)")
    birthday = fields.Char("Năm sinh(*)")
    sex = fields.Selection([('Nam', 'Nam'), ('Nữ', 'Nữ')], string="Giới tính")
    telephone_number = fields.Char("Số ĐT")
    email = fields.Char("Email")
    link_fb = fields.Char("Link FB")
    home = fields.Many2one('manage_candidate.address', string="Quê quán")
    home_now = fields.Many2one('manage_candidate.address', string="Địa chỉ hiện tại")
    school = fields.Many2one('manage_candidate.school', string="Trường đào tạo")
    specialized = fields.Many2one('manage_candidate.specialized', string="Chuyên ngành")
    language = fields.Many2one('manage_candidate.language', "Ngoại ngữ")
    level = fields.Many2one('manage_candidate.level', "Trình độ")
    degree = fields.Selection([('Có', 'Có'), ('Không', 'Không')], string="Chứng chỉ")
    name_job = fields.Many2one('manage_job.manage_job', string="Nguyện vọng")
    orther = fields.Many2one('manage_candidate.orther', string="Lĩnh vực/Khác")
    time_on_home = fields.Date(string="Thời gian về nước")
    experience = fields.Char(string="Kinh nghiệm")
    file = fields.Many2many('manage_candidate.file', string="Hồ sơ")
    note = fields.Text("Ghi chú")
    upload_file = fields.Binary(string="Upload File")
    file_name = fields.Char(string="File Name")
    order_search = fields.Integer(default = 0)
    change_work = fields.Integer(default = 0)
    date_change_work = fields.Date()
    # order_lan = fields.Integer(compute = '_or_lan', store = True)
    order_lan = fields.Integer()

    def change_work_button(self):
        self.change_work = 1
        self.date_change_work = date.today()

    def change_work_cancel(self):
        self.change_work = 0

    # @api.multi
    # @api.depends('level','language')
    # def _or_lan(self):
    #     if 'N1' in str(self.level) or 'N1' in str(self.language):
    #         self.order_lan = 5
    #     elif 'N2' in str(self.level) or 'N2' in str(self.language):
    #         self.order_lan = 4
    #     elif 'N3' in str(self.level) or 'N3' in str(self.language):
    #         self.order_lan = 3
    #     elif 'N4' in str(self.level) or 'N4' in str(self.language):
    #         self.order_lan = 2
    #     elif 'N5' in str(self.level) or 'N5' in str(self.language):
    #         self.order_lan = 1
    #     else:
    #         self.order_lan = 0
    @api.one
    @api.depends('name_candidate', 'telephone_number', 'email', 'school', 'specialized', 'level', 'language',
                 'name_job', 'orther', 'experience', 'note')
    def _tag_search(self):
        strt = str(self.name_candidate) + str(self.language.name) + str(self.note) + str(self.telephone_number) + str(
            self.email) + str(self.school.name) \
               + str(self.specialized.name) + str(self.level.name) + str(self.name_job.name) + str(
            self.orther.name) + str(self.experience)
        self.tag = str(strt)

    tag_ao = fields.Text(compute='_tag_search')
    tag = fields.Text()
    time_on_home_required = fields.Integer()

    @api.onchange('name_job')
    def _timeon(self):
        time_on_home_requireds = self.env['manage_job.manage_job'].search(
            [('name', 'ilike', 'quay lại'), ('id', '=', self.name_job.id)], limit=1)
        self.time_on_home_required = time_on_home_requireds.id

    key_world_can = fields.Char("Từ khóa tìm kiếm")
    key_chuyenmon_can = fields.Many2one('chuyenmon', string="Nguyện vọng ứng tuyển job")
    key_tieng_can = fields.Many2one('tieng', string="Tiếng")
    key_khuvuc_can = fields.Many2one('khuvuc', "Khu vực")
    key_salary = fields.Many2one('luong', string="Mức lương")
    key_khac_can = fields.Many2one('khac', "Khác")

    @api.multi
    def url_candidate(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/render_jobs_list_page?id=%s' % (str(self.id)),
            'target': 'self', }

    @api.multi
    def search_job(self):
        cvs = self.env['jobs.jobs'].sudo().search([])
        for tags in cvs:
            order = 1
            if self.key_nguyenvong:
                if str(self.key_nguyenvong.name) in str(tags.tag):
                    order = order * 3
            if self.key_khuvuc_can:
                if str(self.key_khuvuc_can.name) in str(tags.tag):
                    order = order * 5

            if self.key_khac_can:
                if str(self.key_khac_can.name) in str(tags.tag):
                    order = order + 1

            tags.order_search_can = order


    @api.model
    def create(self, vals):
        record = super(manage_candidate, self).create(vals)
        self.env['candidate_jobs'].create({'candidate_id': record.id})
        return record

class school(models.Model):
    _name = 'manage_candidate.school'
    name = fields.Char()

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res


class specialized(models.Model):
    _name = 'manage_candidate.specialized'
    name = fields.Char()

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res


class language(models.Model):
    _name = 'manage_candidate.language'
    name = fields.Char()

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res


class level(models.Model):
    _name = 'manage_candidate.level'
    name = fields.Char()
    language_name = fields.Many2one('manage_candidate.language', "Ngôn ngữ")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res


class manage_file(models.Model):
    _name = 'manage_candidate.file'
    name = fields.Char()
    # @api.multi
    # def name_get(self):
    #     res = []
    #     for record in self:
    #         resname = []
    #         for namef in record.name:
    #             resname.append((namef))
    #         res.append((record.id, resname))
    #     return res


class manage_orther(models.Model):
    _name = 'manage_candidate.orther'
    name = fields.Char()

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res


class manage_address(models.Model):
    _name = 'manage_candidate.address'
    name = fields.Char()

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res

    _sql_constraints = [
        ('name', 'unique (name)', 'Tỉnh đã tồn tại!')]

class manage_job(models.Model):
    _name = 'manage_job.manage_job'

    name = fields.Char("Nguyện vọng")