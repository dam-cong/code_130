from odoo import models, fields, api
from datetime import date
from pytz import timezone

from fuzzysearch import find_near_matches

class candidate_jobs(models.Model):
    _name = 'candidate_jobs'
    _inherits = {'manage_candidate.manage_candidate': 'candidate_id'}
    _description = 'TTS theo đơn hàng'
    # _rec_name = 'candidate_id'

    candidate_id = fields.Many2one('manage_candidate.manage_candidate', required=True, on_delete='restrict', auto_join=True,
                                string='Danh sách ứng viên', help='Intern-related data of the user', index=True)
    jobs_id = fields.Many2many('jobs.jobs',relation="candidate_jobs_jobs_jobs_rel",  column1="job_id",
                                  column2="candidate_id", string='Job')

    @api.model
    def create(self, vals):
        record = super(candidate_jobs, self).create(vals)
        return record
    promoted = fields.Boolean('Tiến cử')