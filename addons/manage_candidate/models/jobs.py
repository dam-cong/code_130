# -*- coding: utf-8 -*-

from odoo import models, fields, api
from tempfile import TemporaryFile, NamedTemporaryFile
from docx import Document
from docxtpl import DocxTemplate, InlineImage
from io import BytesIO, StringIO
import codecs
import os
import logging

from fuzzysearch import find_near_matches

_logger = logging.getLogger(__name__)
class jobs(models.Model):
    _name = 'jobs.jobs'
    _rec_name = 'name'
    # _order = 'order_lan_job desc'

    manage_candidate = fields.Many2one("manage_candidate.manage_candidate","Ứng viên")
    name = fields.Char("Tên jobs")
    address = fields.Char("Khu vực/ Tỉnh")
    from_company = fields.Many2one('jobs.company_jobs', string="Từ công ty có jobs")
    # type_jobs = fields.Many2one('jobs.type_jobs', string="Chủng loại jobs")
    type_jobs = fields.Selection([('A', 'A'),('B', 'B'), ('C', 'C'), ('D', 'D'),('def', 'Khác')], string="Chủng loại jobs")
    charge_jobs = fields.Many2one('jobs.charge_jobs', string="Người phụ trách jobs")

    amount = fields.Many2one('jobs.amount', "Số lượng")
    amount_recruitment = fields.Char("Số lượng tuyển", relate='amount.amount_recruitment')
    amount_exam = fields.Char("Số lượng thi tuyển", relate='amount.amount_exame')

    older = fields.Char("Độ tuổi")
    sex_required = fields.Selection([('Nam','Nam'),('Nữ','Nữ'),('Nam và nữ', 'Nam và nữ')],"Yêu cầu giới tính")
    work_content = fields.Many2one('jobs.workcontent', "Nội dung công việc")

    benifit = fields.Many2one('jobs.benifit', "Chế độ quyền lợi", limit =  1)
    salary = fields.Char("Lương")
    salary_from = fields.Integer("Tuwf")
    salary_to = fields.Integer("desn")
    reward = fields.Char("Thưởng")
    subsidize = fields.Char("Trợ cấp")
    diff_benifit = fields.Char("Chế độ khác")

    required = fields.Many2one('jobs.required',"Yêu cầu công việc")
    language = fields.Char("Trình độ tiếng")
    education = fields.Char("Trình độ học vấn")
    diff_required = fields.Text("Yêu cầu khác")

    date_recomment = fields.Date("Ngày dự kiến chốt form")
    date_exam = fields.Date("Ngày dự kiến thi tuyển")
    date_sign = fields.Date("Thời gian kí hợp đồng dự kiến")
    date_expectfly = fields.Date("Thời gian xuất cảnh dự kiến")

    note = fields.Many2one('jobs.note',"Ghi chú")
    # tag_ao = fields.Text(compute='_tag_search_jobs')
    # tag_ao = fields.Text(compute = '_tag_jobs')
    tag = fields.Text(compute = '_tag_jobs', store=True)
    order_search_can = fields.Integer(default=0)

    def domain_interns_clone(self):
        intern_clone = self.env['candidate_jobs'].search([('promoted', '=', False)]).ids
        domain = "[('id', 'in', %s)]" % str(intern_clone)
        return domain
    candidates_job = fields.Many2many('candidate_jobs', relation="candidate_jobs_jobs_jobs_rel", column1="job_id",
                                  column2="candidate_id", string="Ứng viên", domain=domain_interns_clone)  # hh_184

    # order_lan_job = fields.Integer(compute = '_order', store=True)
    # @api.multi
    # @api.depends('language')
    # def _order(self):
    #     if 'N1' in str(self.language):
    #         self.order_lan_job = 5
    #     elif 'N2' in str(self.language):
    #         self.order_lan_job = 4
    #     elif 'N3' in str(self.language):
    #         self.order_lan_job = 3
    #     elif 'N4' in str(self.language):
    #         self.order_lan_job = 2
    #     elif 'N5' in str(self.language):
    #         self.order_lan_job = 1
    #     else:
    #         self.order_lan_job = 0
    #     print(self.order_lan_job)
    @api.one
    @api.depends('name', 'address','from_company','charge_jobs', 'work_content','benifit','diff_benifit','required','language','education','diff_required','note',)
    def _tag_jobs(self):
        strt = str(self.name) + str(self.address)+ str(self.from_company.name) + str(self.charge_jobs.name) + \
               str(self.work_content.name) + str(self.benifit.name)+ str(self.diff_benifit)+ str(self.required.name)\
               + str(self.language)+ str(self.education)+ str(self.note.name)
        self.tag = str(strt)

    key_world = fields.Char("Từ khóa tìm kiếm")
    key_chuyenmon = fields.Many2many('chuyenmon',string = "Chuyên môn")
    key_tieng = fields.Many2one('tieng', string = "Tiếng")
    key_khuvuc = fields.Many2one('khuvuc', "Khu vực")
    key_khac = fields.Many2one('khac',"Khác")
    # jobs_candidate = fields.One2many(comodel_name="jobs_candidate", inverse_name="jobs_id",
    #
    #                                 string="Lý lịch làm việc")
    @api.multi
    def url_jobs(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/render_CV_list_page?id=%s' % (str(self.id)),
            'target': 'self', }
    @api.multi
    def search_candidate(self):
        cvs = self.env['manage_candidate.manage_candidate'].sudo().search([])
        for tags in cvs:
            order = 1
            if int(tags.change_work) == 1:
                order = order * 11

            # if "N1" in tags.tag:
            #     order = 5
            # elif "N2" in tags.tag:
            #     order = 4
            # elif "N3" in tags.tag:
            #     order = 3
            # elif "N4" in tags.tag:
            #     order = 2
            # elif "N5" in tags.tag:
            #     order = 1
            arr = []
            if self.key_chuyenmon:
                i = 0
                for item in self.key_chuyenmon:
                    arr.append(item.name)
                    if arr[i] in str(tags.tag):
                        order = order * 3
                        # print(order)
                    i = i +1
            if self.key_tieng:
                if str(self.key_tieng.name) == 'N1':
                    if str('N1') in str(tags.tag):
                        order = order * 13
                elif str(self.key_tieng.name) == 'N2':
                    if str('N1') in str(tags.tag) or str('N2') in str(tags.tag):
                        order = order * 13
                elif str(self.key_tieng.name) == 'N3':
                    if str('N1') in str(tags.tag) or str('N2') in str(tags.tag) or str('N3') in str(tags.tag):
                        order = order * 13
            if self.key_khuvuc:
                if str(self.key_khuvuc.name) in str(tags.tag):
                    order = order * 5
            if self.key_khac:
                if str(self.key_khac.name) in str(tags.tag):
                    order = order * 7

            if order%15015== 0:
                tags.order_search = 10
            elif order%2145== 0 or order%3003 == 0:
                tags.order_search = 9
            elif order%429== 0:
                tags.order_search = 8
            elif order%1365== 0:
                tags.order_search = 7
            elif order%195== 0 or order%455 == 0:
                tags.order_search = 6
            elif order % 39 == 0:
                tags.order_search = 5
            else:
                tags.order_search = 1

    order_search_jobs = fields.Integer(default=0)

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res

    print_world = fields.Many2many('jobs.print_world', string="In world")
    doc = fields.Many2one('jobs.document',"Tải world")

    @api.multi
    def confirm_request(self):
        _logger.info("confirm")
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download_jobs?model=jobs.jobs&id=%s&filename=%s.zip&doc=%s' % (
                str(self.id), str(self.name), str(self.doc.name)),
            'target': 'self', }

    def createWorldDoc(self):
        docs = self.env['jobs.document'].search([('name', '=', "world")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            tpl = DocxTemplate(stream)

            context = {}

            arr_ids = []
            # for ids in self.print_world:
            #     arr_ids.append((ids.id))
            context['name'] = str("Tên jobs")
            context['name_cn'] = self.name
            context['address'] = str("Khu vực/ Tỉnh")
            context['address_cn'] = self.name

            context['amount_recuiment'] = str("Số lượng tuyển")
            context['amount_recuiment_cn'] = self.amount_recruitment

            context['amount_exam'] = str("Số lượng thi tuyển")
            context['amount_exam_cn'] = self.amount_exam

            context['older'] = str("Độ tuổi")
            context['older_cn'] = self.older

            context['sex_required'] = str("Yêu cầu giới tính")
            context['sex_required_cn'] = self.sex_required

            context['work_content'] = str("Nội dung công việc")
            context['work_content_cn'] = self.work_content.name

            context['salary'] = str("Lương")
            context['salary_cn'] = self.salary

            context['reward'] = str("Thưởng")
            context['reward_cn'] = self.reward

            context['subsidize'] = str("Trợ cấp")
            context['subsidize_cn'] = self.subsidize

            context['diff_benifit'] = str("Chế độ khác")
            context['diff_benifit_cn'] = self.benifit.name

            context['language'] = str("Trình độ tiếng")
            context['language_cn'] = self.language

            context['education'] = str("Trình độ học vấn")
            context['education_cn'] = self.education

            context['diff_required'] = str("Yêu cầu khác")
            context['diff_required_cn'] = self.diff_required

            context['date_recomment'] = str("Ngày dự kiến chốt form")
            context['date_recomment_cn'] = self.date_recomment

            context['date_exam'] = str("Ngày dự kiến thi tuyển")
            context['date_exam_cn'] = self.date_exam

            context['date_expectfy'] = str("Thời gian xuất cảnh dự kiến")
            context['date_expectfy_cn'] = self.date_expectfly

            tpl.render(context)

            tempFile = NamedTemporaryFile(delete=False)
            tpl.save(tempFile)
            tempFile.flush()
            tempFile.close()

            return tempFile

    def createWorldDoc2(self):
        docs = self.env['jobs.document'].search([('name', '=', "world2")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            tpl = DocxTemplate(stream)

            context = {}

            context['name'] = str("Tên jobs")
            context['name_cn'] = self.name
            context['address'] = str("Khu vực/ Tỉnh")
            context['address_cn'] = self.name

            context['amount_recuiment'] = str("Số lượng tuyển")
            context['amount_recuiment_cn'] = self.amount_recruitment

            context['amount_exam'] = str("Số lượng thi tuyển")
            context['amount_exam_cn'] = self.amount_exam

            context['older'] = str("Độ tuổi")
            context['older_cn'] = self.older

            context['sex_required'] = str("Yêu cầu giới tính")
            context['sex_required_cn'] = self.sex_required

            context['work_content'] = str("Nội dung công việc")
            context['work_content_cn'] = self.work_content.name

            context['salary'] = str("Lương")
            context['salary_cn'] = self.salary

            context['subsidize'] = str("Trợ cấp")
            context['subsidize_cn'] = self.subsidize

            context['diff_benifit'] = str("Chế độ khác")
            context['diff_benifit_cn'] = self.benifit.name

            context['language'] = str("Trình độ tiếng")
            context['language_cn'] = self.language

            context['education'] = str("Trình độ học vấn")
            context['education_cn'] = self.education

            context['diff_required'] = str("Yêu cầu khác")
            context['diff_required_cn'] = self.diff_required

            context['date_recomment'] = str("Ngày dự kiến chốt form")
            context['date_recomment_cn'] = self.date_recomment

            context['date_exam'] = str("Ngày dự kiến thi tuyển")
            context['date_exam_cn'] = self.date_exam

            context['date_expectfy'] = str("Thời gian xuất cảnh dự kiến")
            context['date_expectfy_cn'] = self.date_expectfly

            tpl.render(context)

            tempFile = NamedTemporaryFile(delete=False)
            tpl.save(tempFile)
            tempFile.flush()
            tempFile.close()

            return tempFile

    def createWorldDoc3(self):
        docs = self.env['jobs.document'].search([('name', '=', "world3")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            tpl = DocxTemplate(stream)

            context = {}

            context['name'] = str("Tên jobs")
            context['name_cn'] = self.name
            context['address'] = str("Khu vực/ Tỉnh")
            context['address_cn'] = self.name

            context['amount_recuiment'] = str("Số lượng tuyển")
            context['amount_recuiment_cn'] = self.amount_recruitment

            context['amount_exam'] = str("Số lượng thi tuyển")
            context['amount_exam_cn'] = self.amount_exam

            context['older'] = str("Độ tuổi")
            context['older_cn'] = self.older

            context['sex_required'] = str("Yêu cầu giới tính")
            context['sex_required_cn'] = self.sex_required

            context['work_content'] = str("Nội dung công việc")
            context['work_content_cn'] = self.work_content.name

            context['salary'] = str("Lương")
            context['salary_cn'] = self.salary

            context['reward'] = str("Trợ cấp")
            context['reward_cn'] = self.subsidize

            context['diff_benifit'] = str("Chế độ khác")
            context['diff_benifit_cn'] = self.benifit.name

            context['language'] = str("Trình độ tiếng")
            context['language_cn'] = self.language

            context['education'] = str("Trình độ học vấn")
            context['education_cn'] = self.education

            context['diff_required'] = str("Yêu cầu khác")
            context['diff_required_cn'] = self.diff_required

            context['date_recomment'] = str("Ngày dự kiến chốt form")
            context['date_recomment_cn'] = self.date_recomment

            context['date_exam'] = str("Ngày dự kiến thi tuyển")
            context['date_exam_cn'] = self.date_exam

            context['date_expectfy'] = str("Thời gian xuất cảnh dự kiến")
            context['date_expectfy_cn'] = self.date_expectfly

            tpl.render(context)

            tempFile = NamedTemporaryFile(delete=False)
            tpl.save(tempFile)
            tempFile.flush()
            tempFile.close()

            return tempFile

    def createWorldDoc4(self):
        docs = self.env['jobs.document'].search([('name', '=', "world4")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            tpl = DocxTemplate(stream)

            context = {}

            arr_ids = []
            context['name'] = str("Tên jobs")
            context['name_cn'] = self.name
            context['address'] = str("Khu vực/ Tỉnh")
            context['address_cn'] = self.name

            context['amount_recuiment'] = str("Số lượng tuyển")
            context['amount_recuiment_cn'] = self.amount_recruitment

            context['amount_exam'] = str("Số lượng thi tuyển")
            context['amount_exam_cn'] = self.amount_exam

            context['older'] = str("Độ tuổi")
            context['older_cn'] = self.older

            context['sex_required'] = str("Yêu cầu giới tính")
            context['sex_required_cn'] = self.sex_required

            context['work_content'] = str("Nội dung công việc")
            context['work_content_cn'] = self.work_content.name

            context['salary'] = str("Lương")
            context['salary_cn'] = self.salary

            context['diff_benifit'] = str("Chế độ khác")
            context['diff_benifit_cn'] = self.benifit.name

            context['language'] = str("Trình độ tiếng")
            context['language_cn'] = self.language

            context['education'] = str("Trình độ học vấn")
            context['education_cn'] = self.education

            context['diff_required'] = str("Yêu cầu khác")
            context['diff_required_cn'] = self.diff_required

            context['date_recomment'] = str("Ngày dự kiến chốt form")
            context['date_recomment_cn'] = self.date_recomment

            context['date_exam'] = str("Ngày dự kiến thi tuyển")
            context['date_exam_cn'] = self.date_exam

            context['date_expectfy'] = str("Thời gian xuất cảnh dựu kiến")
            context['date_expectfy_cn'] = self.date_expectfly

            tpl.render(context)

            tempFile = NamedTemporaryFile(delete=False)
            tpl.save(tempFile)
            tempFile.flush()
            tempFile.close()

            return tempFile
class company_jobs(models.Model):
    _name = 'jobs.company_jobs'

    name = fields.Char("Tên công ty")
    address = fields.Char("Địa chỉ")
    phone_number = fields.Char("Số điện thoại")
    post_office = fields.Char("Số bưu điện")
    fax_number = fields.Char("Số fax")
    representative = fields.Char("Người đại diện")
    responsible_person = fields.Char("Người chịu trách nhiệm")
    number_legal = fields.Char("Số pháp nhân")
    email = fields.Char("Email công ty")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res


class type_jobs(models.Model):
    _name = 'jobs.type_jobs'
    name = fields.Char("Loại jobs")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res


class charge_jobs(models.Model):
    _name = 'jobs.charge_jobs'
    _rec_name = 'name'

    name = fields.Char("Tên người phụ trách jobs")
    id_number = fields.Char("Số chứng minh thư")
    address = fields.Char("Địa chỉ")
    phone_number = fields.Char("Số điện thoại")
    email = fields.Char("Email")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class amount(models.Model):
    _name = 'jobs.amount'
    # name = fields.Char("Số lượng", default = "Số lượng")
    amount_recruitment = fields.Integer("Số lượng tuyển")
    amount_exame = fields.Integer("Số lượng thi tuyển")

class benifit(models.Model):
    _name = 'jobs.benifit'
    name = fields.Text("Khác")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class required(models.Model):
    _name = 'jobs.required'
    name = fields.Text("Khác")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class work_content(models.Model):
    _name = 'jobs.workcontent'
    name = fields.Text("Khác")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class note(models.Model):
    _name = 'jobs.note'
    name = fields.Text("Khác")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class print_world(models.Model):
    _name = 'jobs.print_world'
    name = fields.Char()
