# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception, content_disposition
from io import BytesIO, StringIO
import os
import zipfile
import logging

_logger = logging.getLogger(__name__)


class Jobs(http.Controller):
    @http.route('/web/binary/download_jobs', type='http', auth="public")
    def download_document(self, model, id, filename, doc = None, **kwargs):
        invoice = request.env[model].browse(int(id))
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        if doc == 'world':
            print('1')
            finalDoc = invoice.createWorldDoc()
        elif doc == 'world2':
            print('2')
            finalDoc = invoice.createWorldDoc2()
        elif doc == 'world3':
            print('3')
            finalDoc = invoice.createWorldDoc3()
        elif doc == 'world4':
            print('4')
            finalDoc = invoice.createWorldDoc4()
        name_file = str(filename).split('.')
        if finalDoc is not None:
            archive.write(finalDoc.name, str(name_file[0]+".docx"))
            os.unlink(finalDoc.name)
        else:
            return

        archive.close()
        reponds.flush()
        ret_zip = reponds.getvalue()
        reponds.close()

        return request.make_response(ret_zip,
                                     [('Content-Type', 'application/zip'),
                                      ('Content-Disposition', content_disposition(filename))])
