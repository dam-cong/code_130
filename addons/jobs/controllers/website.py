# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.addons.jobs.models import models
import logging
_logger = logging.getLogger(__name__)

class HrSearch(http.Controller):
    @http.route('/web/binary/render_CV_list_page', type='http', auth='public', website=True)
    def render_CV_list_page(self,id, **kwargs):
        request.env['jobs.jobs'].browse(int(id)).search_candidate()
        invoices = request.env['manage_candidate.manage_candidate'].sudo().search([('order_search','>',6)])
        return http.request.render('jobs.CV_list_page', {'invoices':invoices})

    # @http.route('/list_jobs', type='http', auth='public', website=True)
    # def render_jobs_list_page(self, **kwargs):
    #     invoices = request.env['jobs.jobs'].sudo().search([])
    #     print(len(invoices))
    #     return http.request.render('hr_search.jobs_list_page', {'invoices': invoices})