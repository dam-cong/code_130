# -*- coding: utf-8 -*-

from odoo import models, fields, api
from tempfile import TemporaryFile, NamedTemporaryFile
from docx import Document
from docxtpl import DocxTemplate, InlineImage
from io import BytesIO, StringIO
import codecs
import os
import logging

from fuzzysearch import find_near_matches

_logger = logging.getLogger(__name__)
class jobs(models.Model):
    _name = 'jobs.jobs'

    name = fields.Char("Tên job")
    address = fields.Char("Khu vực/ Tỉnh")
    from_company = fields.Many2one('jobs.company_job', string="Từ công ty có Jobs")
    # type_job = fields.Many2one('jobs.type_job', string="Chủng loại Jobs")
    type_job = fields.Selection([('A', 'A'),('B', 'B'), ('C', 'C'), ('D', 'D'),('def', 'Khác')], string="Chủng loại Jobs")
    charge_job = fields.Many2one('jobs.charge_job', string="Người phụ trách Jobs")

    amount = fields.Many2one('jobs.amount', "Số lượng")
    amount_recruitment = fields.Char("Số lượng tuyển", relate='amount.amount_recruitment')
    amount_exam = fields.Char("Số lượng thi tuyển", relate='amount.amount_exame')

    older = fields.Char("Độ tuổi")
    sex_required = fields.Selection([('Nam','Nam'),('Nữ','Nữ'),('Nam và nữ', 'Nam và nữ')],"Yêu cầu giới tính")
    work_content = fields.Many2one('jobs.workcontent', "Nội dung công việc")

    benifit = fields.Many2one('jobs.benifit', "Chế độ quyền lợi", limit =  1)
    salary = fields.Char("Lương")
    reward = fields.Char("Thưởng")
    subsidize = fields.Char("Trợ cấp")
    diff_benifit = fields.Char("Chế độ khác")

    required = fields.Many2one('jobs.required',"Yêu cầu công việc")
    language = fields.Char("Trình độ tiếng")
    education = fields.Char("Trình độ học vấn")
    diff_required = fields.Text("Yêu cầu khác")

    date_recomment = fields.Date("Ngày dự kiến chốt form")
    date_exam = fields.Date("Ngày dự kiến thi tuyển")
    date_sign = fields.Date("Thời gian kí hợp đồng dự kiến")
    date_expectfly = fields.Date("Thời gian xuất cảnh dự kiến")

    note = fields.Many2one('jobs.note',"Ghi chú")
    # tag_ao = fields.Text(compute='_tag_search_job')
    tag_ao = fields.Text(compute = '_tag_job')
    tag = fields.Text()
    order_search_can = fields.Integer(default=0)

    @api.one
    @api.depends('note', 'name', 'address','work_content')
    def _tag_job(self):
        strt = str(self.note.name) + str(self.name)+ str(self.address) + str(self.work_content.name)
        self.tag = str(strt)

    key_world = fields.Char("Từ khóa tìm kiếm")
    key_chuyenmon = fields.Many2one('chuyenmon',string = "Chuyên môn")
    key_tieng = fields.Many2one('tieng', string = "Tiếng")
    key_khuvuc = fields.Many2one('khuvuc', "Khu vực")
    key_khac = fields.Many2one('khac',"Khác")
    # job_candidate = fields.One2many(comodel_name="job_candidate", inverse_name="job_id",
    #
    #                                 string="Lý lịch làm việc")
    @api.multi
    def url_jobs(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/render_CV_list_page?id=%s' % (str(self.id)),
            'target': 'self', }
    @api.multi
    def search_candidate(self):
        cvs = self.env['manage_candidate.manage_candidate'].sudo().search([])
        for tags in cvs:
            order = 0
            if int(tags.change_work) == 1:
                order = order + 7
            # for i in range(1, 4):
            #     if self.key_chuyenmon:
            #         arr1 = find_near_matches(str(self.key_chuyenmon.name), str(tags.tag), max_l_dist=i)
            #         if arr1:
            #             order = order + 5
            #             break
            if self.key_chuyenmon:
                if str(self.key_chuyenmon.name) in str(tags.tag):
                    order = order + 7
            if self.key_tieng:
                if str(self.key_tieng.name) in str(tags.tag):
                    order = order + 3
            if self.key_khuvuc:
                if str(self.key_khuvuc.name) in str(tags.tag):
                    order = order + 2
            # for i in range(1, 4):
            #     if self.key_khuvuc:
            #         arr2 = find_near_matches(str(self.key_khuvuc.name), str(tags.tag), max_l_dist=i)
            #         if arr2:
            #             order = order + 1
            #             break
            if self.key_khac:
                if str(self.key_khac.name) in str(tags.tag):
                    order = order + 1
            # for i in range(1, 4):
            #     if self.key_khac:
            #         arr3 = find_near_matches(str(self.key_khac.name), str(tags.tag), max_l_dist=i)
            #         if arr3:
            #             order = order + 1
            #             break
            tags.order_search = order

    order_search_job = fields.Integer(default=0)

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res

    print_world = fields.Many2many('jobs.print_world', string="In world")
    doc = fields.Many2one('jobs.document',"Tải world")

    @api.multi
    def confirm_request(self):
        _logger.info("confirm")
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/download_jobs?model=jobs.jobs&id=%s&filename=%s.zip&doc=%s' % (
                str(self.id), str(self.name), str(self.doc.name)),
            'target': 'self', }

    def createWorldDoc(self):
        docs = self.env['jobs.document'].search([('name', '=', "world")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            tpl = DocxTemplate(stream)

            context = {}

            arr_ids = []
            # for ids in self.print_world:
            #     arr_ids.append((ids.id))
            context['name'] = str("Tên job")
            context['name_cn'] = self.name
            context['address'] = str("Khu vực/ Tỉnh")
            context['address_cn'] = self.name

            context['amount_recuiment'] = str("Số lượng tuyển")
            context['amount_recuiment_cn'] = self.amount_recruitment

            context['amount_exam'] = str("Số lượng thi tuyển")
            context['amount_exam_cn'] = self.amount_exam

            context['older'] = str("Độ tuổi")
            context['older_cn'] = self.older

            context['sex_required'] = str("Yêu cầu giới tính")
            context['sex_required_cn'] = self.sex_required

            context['work_content'] = str("Nội dung công việc")
            context['work_content_cn'] = self.work_content.name

            context['salary'] = str("Lương")
            context['salary_cn'] = self.salary

            context['reward'] = str("Thưởng")
            context['reward_cn'] = self.reward

            context['subsidize'] = str("Trợ cấp")
            context['subsidize_cn'] = self.subsidize

            context['diff_benifit'] = str("Chế độ khác")
            context['diff_benifit_cn'] = self.benifit.name

            context['language'] = str("Trình độ tiếng")
            context['language_cn'] = self.language

            context['education'] = str("Trình độ học vấn")
            context['education_cn'] = self.education

            context['diff_required'] = str("Yêu cầu khác")
            context['diff_required_cn'] = self.diff_required

            context['date_recomment'] = str("Ngày dự kiến chốt form")
            context['date_recomment_cn'] = self.date_recomment

            context['date_exam'] = str("Ngày dự kiến thi tuyển")
            context['date_exam_cn'] = self.date_exam

            context['date_expectfy'] = str("Thời gian xuất cảnh dự kiến")
            context['date_expectfy_cn'] = self.date_expectfly

            tpl.render(context)

            tempFile = NamedTemporaryFile(delete=False)
            tpl.save(tempFile)
            tempFile.flush()
            tempFile.close()

            return tempFile

    def createWorldDoc2(self):
        docs = self.env['jobs.document'].search([('name', '=', "world2")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            tpl = DocxTemplate(stream)

            context = {}

            context['name'] = str("Tên job")
            context['name_cn'] = self.name
            context['address'] = str("Khu vực/ Tỉnh")
            context['address_cn'] = self.name

            context['amount_recuiment'] = str("Số lượng tuyển")
            context['amount_recuiment_cn'] = self.amount_recruitment

            context['amount_exam'] = str("Số lượng thi tuyển")
            context['amount_exam_cn'] = self.amount_exam

            context['older'] = str("Độ tuổi")
            context['older_cn'] = self.older

            context['sex_required'] = str("Yêu cầu giới tính")
            context['sex_required_cn'] = self.sex_required

            context['work_content'] = str("Nội dung công việc")
            context['work_content_cn'] = self.work_content.name

            context['salary'] = str("Lương")
            context['salary_cn'] = self.salary

            context['subsidize'] = str("Trợ cấp")
            context['subsidize_cn'] = self.subsidize

            context['diff_benifit'] = str("Chế độ khác")
            context['diff_benifit_cn'] = self.benifit.name

            context['language'] = str("Trình độ tiếng")
            context['language_cn'] = self.language

            context['education'] = str("Trình độ học vấn")
            context['education_cn'] = self.education

            context['diff_required'] = str("Yêu cầu khác")
            context['diff_required_cn'] = self.diff_required

            context['date_recomment'] = str("Ngày dự kiến chốt form")
            context['date_recomment_cn'] = self.date_recomment

            context['date_exam'] = str("Ngày dự kiến thi tuyển")
            context['date_exam_cn'] = self.date_exam

            context['date_expectfy'] = str("Thời gian xuất cảnh dự kiến")
            context['date_expectfy_cn'] = self.date_expectfly

            tpl.render(context)

            tempFile = NamedTemporaryFile(delete=False)
            tpl.save(tempFile)
            tempFile.flush()
            tempFile.close()

            return tempFile

    def createWorldDoc3(self):
        docs = self.env['jobs.document'].search([('name', '=', "world3")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            tpl = DocxTemplate(stream)

            context = {}

            context['name'] = str("Tên job")
            context['name_cn'] = self.name
            context['address'] = str("Khu vực/ Tỉnh")
            context['address_cn'] = self.name

            context['amount_recuiment'] = str("Số lượng tuyển")
            context['amount_recuiment_cn'] = self.amount_recruitment

            context['amount_exam'] = str("Số lượng thi tuyển")
            context['amount_exam_cn'] = self.amount_exam

            context['older'] = str("Độ tuổi")
            context['older_cn'] = self.older

            context['sex_required'] = str("Yêu cầu giới tính")
            context['sex_required_cn'] = self.sex_required

            context['work_content'] = str("Nội dung công việc")
            context['work_content_cn'] = self.work_content.name

            context['salary'] = str("Lương")
            context['salary_cn'] = self.salary

            context['reward'] = str("Trợ cấp")
            context['reward_cn'] = self.subsidize

            context['diff_benifit'] = str("Chế độ khác")
            context['diff_benifit_cn'] = self.benifit.name

            context['language'] = str("Trình độ tiếng")
            context['language_cn'] = self.language

            context['education'] = str("Trình độ học vấn")
            context['education_cn'] = self.education

            context['diff_required'] = str("Yêu cầu khác")
            context['diff_required_cn'] = self.diff_required

            context['date_recomment'] = str("Ngày dự kiến chốt form")
            context['date_recomment_cn'] = self.date_recomment

            context['date_exam'] = str("Ngày dự kiến thi tuyển")
            context['date_exam_cn'] = self.date_exam

            context['date_expectfy'] = str("Thời gian xuất cảnh dự kiến")
            context['date_expectfy_cn'] = self.date_expectfly

            tpl.render(context)

            tempFile = NamedTemporaryFile(delete=False)
            tpl.save(tempFile)
            tempFile.flush()
            tempFile.close()

            return tempFile

    def createWorldDoc4(self):
        docs = self.env['jobs.document'].search([('name', '=', "world4")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].attachment, "base64"))
            tpl = DocxTemplate(stream)

            context = {}

            arr_ids = []
            context['name'] = str("Tên job")
            context['name_cn'] = self.name
            context['address'] = str("Khu vực/ Tỉnh")
            context['address_cn'] = self.name

            context['amount_recuiment'] = str("Số lượng tuyển")
            context['amount_recuiment_cn'] = self.amount_recruitment

            context['amount_exam'] = str("Số lượng thi tuyển")
            context['amount_exam_cn'] = self.amount_exam

            context['older'] = str("Độ tuổi")
            context['older_cn'] = self.older

            context['sex_required'] = str("Yêu cầu giới tính")
            context['sex_required_cn'] = self.sex_required

            context['work_content'] = str("Nội dung công việc")
            context['work_content_cn'] = self.work_content.name

            context['salary'] = str("Lương")
            context['salary_cn'] = self.salary

            context['diff_benifit'] = str("Chế độ khác")
            context['diff_benifit_cn'] = self.benifit.name

            context['language'] = str("Trình độ tiếng")
            context['language_cn'] = self.language

            context['education'] = str("Trình độ học vấn")
            context['education_cn'] = self.education

            context['diff_required'] = str("Yêu cầu khác")
            context['diff_required_cn'] = self.diff_required

            context['date_recomment'] = str("Ngày dự kiến chốt form")
            context['date_recomment_cn'] = self.date_recomment

            context['date_exam'] = str("Ngày dự kiến thi tuyển")
            context['date_exam_cn'] = self.date_exam

            context['date_expectfy'] = str("Thời gian xuất cảnh dựu kiến")
            context['date_expectfy_cn'] = self.date_expectfly

            tpl.render(context)

            tempFile = NamedTemporaryFile(delete=False)
            tpl.save(tempFile)
            tempFile.flush()
            tempFile.close()

            return tempFile
class company_job(models.Model):
    _name = 'jobs.company_job'

    name = fields.Char("Tên công ty")
    address = fields.Char("Địa chỉ")
    phone_number = fields.Char("Số điện thoại")
    post_office = fields.Char("Số bưu điện")
    fax_number = fields.Char("Số fax")
    representative = fields.Char("Người đại diện")
    responsible_person = fields.Char("Người chịu trách nhiệm")
    number_legal = fields.Char("Số pháp nhân")
    email = fields.Char("Email công ty")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res


class type_job(models.Model):
    _name = 'jobs.type_job'
    name = fields.Char("Loại job")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res


class charge_job(models.Model):
    _name = 'jobs.charge_job'
    _rec_name = 'name'

    name = fields.Char("Tên người phụ trách job")
    id_number = fields.Char("Số chứng minh thư")
    address = fields.Char("Địa chỉ")
    phone_number = fields.Char("Số điện thoại")
    email = fields.Char("Email")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class amount(models.Model):
    _name = 'jobs.amount'
    # name = fields.Char("Số lượng", default = "Số lượng")
    amount_recruitment = fields.Integer("Số lượng tuyển")
    amount_exame = fields.Integer("Số lượng thi tuyển")

class benifit(models.Model):
    _name = 'jobs.benifit'
    name = fields.Text("Khác")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class required(models.Model):
    _name = 'jobs.required'
    name = fields.Text("Khác")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class work_content(models.Model):
    _name = 'jobs.workcontent'
    name = fields.Text("Khác")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class note(models.Model):
    _name = 'jobs.note'
    name = fields.Text("Khác")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class print_world(models.Model):
    _name = 'jobs.print_world'
    name = fields.Char()
