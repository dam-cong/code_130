# -*- coding: utf-8 -*-

from odoo import models, fields, api

class search_chuyenmon(models.Model):
    _name = 'chuyenmon'
    name = fields.Char("Chuyên môn")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class search_tieng(models.Model):
    _name = 'tieng'
    name = fields.Char("Ngoại ngữ")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class search_khuvuc(models.Model):
    _name = 'khuvuc'
    name = fields.Char("Khu vực")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res
class search_khac(models.Model):
    _name = 'khac'
    name = fields.Char("Khác")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name))
        return res

class mydocument(models.Model):
    _name = 'jobs.document'
    _description = u'Các loại văn bản, báo cáo'
    _rec_name = 'name'

    name = fields.Char("Tên", required=True)  # hh_104
    name_view = fields.Char("Tên hiển thị", required = True)
    note = fields.Text("Ghi chú")  # hh_105
    attachment = fields.Binary('Văn bản mẫu', required=True)  # hh_106

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name_view))
        return res
