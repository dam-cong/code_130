
from odoo import models, fields, api
from datetime import date
from pytz import timezone


class job_candidate(models.Model):
    _name = 'job_candidate'
    _inherit = {'manage_candidate.manage_candidate': 'candidate_id'}
    job_id = fields.Many2one('jobs.jobs')
    candidate_id = fields.Many2one('manage_candidate.manage_candidate', auto_join = True)
    # order_search = fields.Integer(default=0)
    # change_work = fields.Integer(default=0)