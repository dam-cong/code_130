# -*- coding: utf-8 -*-

from odoo import models, fields, api

class syndication(models.Model):
    _name = 'syndication.syndication'

    name_japanese = fields.Char("Tên khách hàng tiếng Nhật")
    name_vietnam = fields.Char("Tên khách hàng tiếng phiên âm")
    province = fields.Char("Tỉnh")
    status = fields.Selection([('1', 'Đang tiếp cận'), ('2', 'Thị sát'), ('3', 'Ký hiệp định'), ('4', 'Đang tuyển'),('5','Đã từng hợp tác nhưng hiện đã dừng')], string="Trạng thái", required = True)
    note = fields.Text("Ghi chú")
    employee = fields.Many2one('employees.employees', store=True)
    department = fields.Many2one('employees.department','Phòng Ban', store=True)
    name_type = fields.Many2one('syndication.type', 'Phân loại khách hàng')
    _sql_constraints = [
        ('name_japanese', 'unique (name_japanese)', 'Tên nghiệp đoàn đã tồn tại!'),('name_vietnam', 'unique (name_vietnam)', 'Tên nghiệp đoàn đã tồn tại!')]

    @api.onchange('status')
    def _user(self):
        if self.status:
            print(self.env.user.login)
            employee_rec = self.env['employees.employees'].search([('email', '=', self.env.user.login)], limit=1)
            self.employee = employee_rec.id
            print(self.employee.id)
            self.department = employee_rec.department

class type(models.Model):
    _name = 'syndication.type'
    name = fields.Char("Tên loại khách hàng")
    _sql_constraints = [
        ('name', 'unique (name)', 'Tên loại khách hàng đã được đăng kí!')]