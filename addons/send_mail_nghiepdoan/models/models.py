# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date


class send_mail_tempcandidate(models.Model):
    _name = 'send_mail_nghiepdoan.send_mail_nghiepdoan'

    @api.model
    def _cron_do_task(self):
        sql = "select name_japanese,name_vietnam,syndication_type.name,employees_employees.name,syndication_syndication.create_date "\
                "from syndication_syndication inner join employees_employees on syndication_syndication.employee  = employees_employees.id"\
				" inner join syndication_type on syndication_syndication.name_type = syndication_type.id"\
                " where syndication_syndication.create_date = CURRENT_DATE" \
                " or syndication_syndication.create_date = CURRENT_DATE - 1" \
                " or syndication_syndication.create_date = CURRENT_DATE - 2"\
                " or syndication_syndication.create_date = CURRENT_DATE - 3"\
                " or syndication_syndication.create_date = CURRENT_DATE - 4"\
                " or syndication_syndication.create_date = CURRENT_DATE - 5"\
                " or syndication_syndication.create_date = CURRENT_DATE - 6"
        self._cr.execute(sql)
        id_needed = self._cr.fetchall()
        data_mj = []
        for item in range(len(id_needed)):
            if(id_needed[item][1] != 0):
                data_mj.append({
                    'name_japanese': id_needed[item][0],
                    'name_vietnam': id_needed[item][1],
                    'name_type': id_needed[item][2],
                    'employee': id_needed[item][3],
                    'create_date': id_needed[item][4],
                })

        string = '<table style="border: 1px solid #333;border-collapse: collapse; white-space: norwarp;"><tr>' \
                 '<td style="width:45px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">STT</td>' \
                 '<td style="width:140px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">TÊN KHÁCH HÀNG TIẾNG NHẬT</td>' \
                 '<td style="width:140px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">TÊN KHÁCH HÀNG PHIÊN ÂM</td>' \
                 '<td style="width:80px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">LOẠI KH</td>' \
                 '<td style="width:80px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">BỘ PHẬN CẬP NHẬT</td>' \
                 '<td style="width:80px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">NGÀY CẬP NHẬT</td>' \
                 '<td style="width:60px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">GHI CHÚ</td>' \
                 '</tr>'
        i = 0;
        for d in data_mj:
            i = i + 1
            string += '<tr><td style="width:45px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
                i) + '</td> '
            string += '<td style="width:140px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
                d.get('name_japanese')) + '</td>'
            string += '<td style="width:140px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
                d.get('name_vietnam')) + '</td>'
            string += '<td style="width:80px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
                d.get('name_type')) + '</td>'
            string += '<td style="width:80px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
                d.get('employee')) + '</td>'
            string += '<td style="width:80px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
                d.get('create_date')) + '</td>'
            string += '<td style="width:60px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman"></td></tr></table>'
        mail = self.env.ref('mail_template_nghiepdoan.mail_dskh_update')
        origin_mail = mail.body_html
        mail.body_html = origin_mail.replace('{data_candidate}', string)
        return origin_mail

    @api.model
    def cron_do_task(self):
        origin_mail = self._cron_do_task()
        template = self.env.ref('mail_template_nghiepdoan.mail_dskh_update')
        # origin_mail_begin = template.body_html
        # template.body_html = origin_mail
        print(template.id)
        ketqua = template.send_mail(1, force_send=True)
        print(ketqua)
        template.body_html = origin_mail

    @api.model
    def _cron_do_task_total(self):
        sql = "SELECT employees_employees.stt,employees_employees.name,sum(CASE WHEN syndication_syndication.name_type = '1' THEN 1 ELSE '0' END) AS nd,sum(CASE WHEN syndication_syndication.name_type = '2' THEN 1 ELSE '0' END) AS xn,sum(CASE WHEN syndication_syndication.name_type = '3' THEN 1 ELSE '0' END) AS ha,sum(CASE WHEN syndication_syndication.name_type != '1' and syndication_syndication.name_type != '2' and syndication_syndication.name_type != '3' THEN 1 ELSE '0' END) AS other,"\
	        "sum(CASE WHEN syndication_syndication.name_type = '1' THEN 1 ELSE '0' END) +"\
 	        "sum(CASE WHEN syndication_syndication.name_type = '2' THEN 1 ELSE '0' END) +"\
 	        "sum(CASE WHEN syndication_syndication.name_type = '3' THEN 1 ELSE '0' END) +" \
            "sum(CASE WHEN syndication_syndication.name_type != '1' and syndication_syndication.name_type != '2' and syndication_syndication.name_type != '3' THEN 1 ELSE '0' END) as total"\
            " FROM syndication_syndication"\
            " inner join employees_employees on syndication_syndication.employee  = employees_employees.id" \
            " where status = '1' or status = '2' or status = '3' or status = '4'" \
            " GROUP BY employees_employees.stt,employees_employees.name " \
            " order by employees_employees.stt asc"

        self._cr.execute(sql)
        id_needed = self._cr.fetchall()
        data_mj = []
        for item in range(len(id_needed)):
            if (id_needed[item][1] != 0):
                data_mj.append({
                    'name': id_needed[item][1],
                    'nd': id_needed[item][2],
                    'xn': id_needed[item][3],
                    'ha': id_needed[item][4],
                    'other': id_needed[item][5],
                    'total': id_needed[item][6],
                })

        string = '<table style="border: 1px solid #333;border-collapse: collapse; white-space: norwarp;"><tr>' \
                 '<td style="width:45px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">STT</td>' \
                 '<td style="width:100px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">BỘ PHẬN</td>' \
                 '<td style="width:100px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">TỔNG SỐ NĐ ĐÃ ĐĂNG KÝ</td>' \
                 '<td style="width:100px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">TỔNG SỐ XN ĐÃ ĐĂNG KÝ</td>' \
                 '<td style="width:100px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">TỔNG SỐ HAKEN ĐÃ ĐĂNG KÝ</td>' \
                 '<td style="width:100px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">TỔNG SỐ KH KHÁC ĐÃ ĐĂNG KÝ</td>' \
                 '<td style="width:100px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">TỔNG SỐ</td>' \
                 '<td style="width:100px;text-align: center;vertical-align: text-top;border: 1px solid #333; color:#000;font-family: TimeNewRoman">GHI CHÚ</td>' \
                 '</tr>'
        i = 0
        total_nd = 0
        total_xn = 0
        total_ha = 0
        total_other = 0
        total_total = 0
        for d in data_mj:
            i = i + 1
            total_nd = total_nd + d.get('nd')
            total_xn = total_xn + d.get('xn')
            total_ha = total_ha + d.get('ha')
            total_other = total_other + d.get('other')
            total_total = total_total + d.get('total')
            string += '<tr><td style="width:45px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
                i) + '</td>'
            string += '<td style="width:100px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
                d.get('name')) + '</td>'
            string += '<td style="width:100px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
                d.get('nd')) + '</td>'
            string += '<td style="width:100px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
                d.get('xn')) + '</td>'
            string += '<td style="width:100px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
                d.get('ha')) + '</td>'
            string += '<td style="width:100px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
                d.get('other')) + '</td>'
            string += '<td style="width:100px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
                d.get('total')) + '</td>'
            string += '<td style="width:100px;text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman"></td></tr>'

        string += '<tr><td style="text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman"></td>'
        string += '<td style="text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">TỔNG</td>'
        string += '<td style="text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
            total_nd) + '</td>'
        string += '<td style="text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
            total_xn) + '</td>'
        string += '<td style="text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
            total_ha) + '</td>'
        string += '<td style="text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
            total_other) + '</td>'
        string += '<td style="text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman">' + str(
            total_total) + '</td>'
        string += '<td style="text-align: center;border: 1px solid #333; color:#000;font-family: TimeNewRoman"></td></tr></table>'
        mail = self.env.ref('mail_template_nghiepdoan.mail_total_update')
        origin_mail = mail.body_html
        mail.body_html = origin_mail.replace('{data_candidate}', string)
        return origin_mail

    @api.model
    def cron_do_task_total(self):
        origin_mail = self._cron_do_task_total()
        template = self.env.ref('mail_template_nghiepdoan.mail_total_update')
        # origin_mail_begin = template.body_html
        # template.body_html = origin_mail
        print(template.id)
        ketqua = template.send_mail(1, force_send=True)
        print(ketqua)
        template.body_html = origin_mail


